# Docker ELK Apache logs

### Dev Setup

To setup your dev environment, run ```./setup-dev.sh```. This will download the latest log file(s) from the repo, start an ELK environment using docker-compose and import all the data ready for querying. Once complete go to http://localhost:5601 to get into the Kibana front-end.

You can also ```source .aliases``` for some shortcuts if you like:

* ```dcreset```: stop and remove current containers and start new ones
* ```dcstop```: ```docker-compose stop```
* ```dcup```: ```docker-compose up```
* ```deshell```: open a shell on the specified container (e.g. ```deshell mintel_logstash_1```)
* ```dps```: ```docker ps```

To load the visualisations/dashboards, go to Management > Saved Objects > Import and select the included export.json file.

### Ratio of Error vs Successful requests

Since I was asked for the ratio here I went for a pie chart to quickly show this. Successful requests include the standard 2xx codes plus redirects (3xx). Errors include response codes from 400-599 (client and server-side).

### Number of requests for each URL

The number of URLs on a production website is usually pretty extensive so I figured a table of items showing URLs & no. of requests would be more appropriate that graphing this.

### The distribution of client Operating Systems and Browsers

I would want to have the OS/Browser version displayed as well as the name so rather than putting both fields into the table, which looked a little messy, I created a couple of scripted fields, 'osandversion' and 'browserandversion' concatenating the fields I wanted to display. Showing this as a table is simply my preference but you could just as easily display this as a pie chart.