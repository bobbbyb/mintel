#!/usr/bin/env bash

set -e

if [[ ! -d logs ]]; then
    mkdir -p logs
    cd logs/
    wget https://github.com/mintel/sre-interview-assets/raw/master/challenges/challenge_01_docker_elk_apache_logs/WEB_access_log.log.gz
    gunzip WEB_access_log.log.gz
    cd ..
fi

docker-compose stop && docker-compose rm -f && docker-compose up -d
